export const DEFAULT_DURATION = 5;
export const films = {
    elki1: 1,
    elki2: 2,
    '1': 1,
    '2': 2,
}

const SKIP = 'skip';
const TEXT = 'text';
const PHOTO = 'photo';
const PHOTO_TEXT = 'photoText';
const BUTTON_TEXT = 'buttonText';
const COMBINED = 'combined';

const sequence = [{},
    {
        episode: '01',
        src: 'https://ongocinemamedia-euno.streaming.media.azure.net//39dbb93c-1f60-4f84-9f08-7057b81200b9/YOLKI-1_ORIGINAL_25fps_1920x800p.ism/manifest',
        title: 'Елки',
        activeElements: {
            // '0:00:00': { // example fields
            //     button: '',
            //     title: '',
            //     text: '',
            //     type: '',
            //     duration: 3.00 || DEFAULT_DURATION,
            //     image: '',
            //     href: '',
            //     targetTime: '',
            //     width: 10.0000,
            //     left: 10.0000,
            //     top: 65.0000,
            // },
            '0:01:08': {
                title: 'Новелла 1',
                type: SKIP,
                targetTime: '0:04:01',
            },
            '0:01:54': {
                title: 'Кинешма',
                text: 'Узнай где этот город',
                type: COMBINED,
                image: 'p01',
                href: 'https://ru.wikipedia.org/wiki/Кинешма',
                button: 'Карта',
            },
            '0:02:30': {
                title: 'КП',
                text: 'Комсомольская правда — советская и российская ежедневная общественно-политическая газета',
                type: PHOTO_TEXT,
                image: 'p02',
                href: 'https://www.msk.kp.ru/',
            },
            '0:03:30': {
                title: 'Обращения',
                text: 'Узнай историю новогодних обращений',
                type: BUTTON_TEXT,
                href: 'https://ru.wikipedia.org/wiki/Новогоднее_обращение_к_народу',
                button: 'Читать',
            },
            '0:04:01': {
                title: 'Новелла 2',
                type: SKIP,
                targetTime: '0:06:14',
            },
            '0:06:14': {
                title: 'Новелла 3',
                type: SKIP,
                targetTime: '0:08:10',
            },
            '0:07:30': {
                title: 'Брежнева',
                type: PHOTO,
                image: 'p03',
                href: 'https://www.youtube.com/watch?v=ma0FuBGgK_E',
            },
            '0:08:10': {
                title: 'Новелла 4',
                type: SKIP,
                targetTime: '0:10:09',
            },
            '0:10:09': {
                title: 'Новелла 5',
                type: SKIP,
                targetTime: '0:11:28',
            },
            '0:10:30': {
                title: 'Перекрёсток',
                text: 'Перекрёсток',
                type: COMBINED,
                image: 'p04',
                href: 'http://perekrestok.ru',
                button: 'За покупками',
            },
            '0:11:28': {
                title: 'Новелла 6',
                type: SKIP,
                targetTime: '0:16:09',
            },
            '0:16:09': {
                title: 'Новелла 7',
                type: SKIP,
                targetTime: '0:18:22',
            },
            '0:16:20': {
                title: 'S7',
                type: PHOTO,
                image: 'p05',
                href: 'https://www.s7.ru',
            },
            '0:18:22': {
                title: 'Новелла 8',
                type: SKIP,
                targetTime: '0:22:24',
            },
            '0:18:40': {
                title: 'Бавлы',
                text: 'Узнай где этот город',
                type: COMBINED,
                image: 'p08',
                href: 'https://ru.wikipedia.org/wiki/Бавлы',
                button: 'Карта',
            },
            '0:22:24': {
                title: 'Новелла 9',
                type: SKIP,
                targetTime: '0:27:30',
            },
            '0:24:43': {
                title: 'Нойз',
                text: 'Noize MC — Устрой Дестрой!',
                type: COMBINED,
                image: 'p06',
                href: 'https://music.youtube.com/playlist?list=OLAK5uy_n2K13fijDN4SzpaBWJGYzkfx3P_YroDNc',
                button: 'Слушать трек',
            },
            '0:27:30': {
                title: 'Новелла 10',
                type: SKIP,
                targetTime: '0:29:52',
            },
            '0:29:52': {
                title: 'Новелла 11',
                type: SKIP,
                targetTime: '0:30:38',
            },
            '0:30:38': {
                title: 'Новелла 12',
                type: SKIP,
                targetTime: '0:34:08',
            },
            '0:34:08': {
                title: 'Новелла 13',
                type: SKIP,
                targetTime: '0:38:00',
            },
            '0:38:00': {
                title: 'Новелла 14',
                type: SKIP,
                targetTime: '0:39:25',
            },
            '0:39:25': {
                title: 'Новелла 15',
                type: SKIP,
                targetTime: '0:41:02',
            },
            '0:40:00': {
                title: 'Связной',
                text: 'Всё для Нового года!',
                type: COMBINED,
                image: 'p07',
                href: 'https://www.svyaznoy.ru/special-offers/4008957',
                button: 'За подарками!',
            },
            '0:41:02': {
                title: 'Новелла 16',
                type: SKIP,
                targetTime: '0:42:22',
            },
            '0:42:22': {
                title: 'Новелла 17',
                type: SKIP,
                targetTime: '0:48:00',
            },
            '0:48:00': {
                title: 'Новелла 18',
                type: SKIP,
                targetTime: '0:51:03',
            },
            '0:48:55': {
                title: 'Курение',
                text: 'Курение вредит вашему здоровью',
                type: TEXT,
            },
            '0:51:03': {
                title: 'Новелла 19',
                type: SKIP,
                targetTime: '0:52:20',
            },
            '0:52:20': {
                title: 'Новелла 20',
                type: SKIP,
                targetTime: '0:56:02',
            },
            '0:56:02': {
                title: 'Новелла 21',
                type: SKIP,
                targetTime: '0:56:29',
            },
            '0:56:29': {
                title: 'Новелла 22',
                type: SKIP,
                targetTime: '0:56:51',
            },
            '0:56:51': {
                title: 'Новелла 23',
                type: SKIP,
                targetTime: '0:58:12',
            },
            '0:58:12': {
                title: 'Новелла 24',
                type: SKIP,
                targetTime: '1:00:40',
            },
            '0:58:45': {
                title: 'Макдоналдс',
                type: PHOTO,
                image: 'p09',
                href: 'https://vk.com/mcdonaldsrussia?w=wall-9580285_537300',
            },
            '1:07:00': {
                title: 'Приметы',
                text: 'Новогодние приметы',
                type: TEXT,
                href: 'https://vsyamagik.ru/novogodnie-primety-sueveriya-i-tradicii/',
            },
            '1:00:40': {
                title: 'Новелла 25',
                type: SKIP,
                targetTime: '1:01:25',
            },
            '1:01:25': {
                title: 'Новелла 26',
                type: SKIP,
                targetTime: '1:02:42',
            },
            '1:02:42': {
                title: 'Новелла 27',
                type: SKIP,
                targetTime: '1:05:00',
            },
            '1:05:00': {
                title: 'Новелла 28',
                type: SKIP,
                targetTime: '1:07:10',
            },
            '1:07:10': {
                title: 'Новелла 29',
                type: SKIP,
                targetTime: '1:08:56',
            },
            '1:08:56': {
                title: 'Новелла 30',
                type: SKIP,
                targetTime: '1:11:07',
            },
            '1:11:07': {
                title: 'Новелла 31',
                type: SKIP,
                targetTime: '1:12:27',
            },
            '1:12:27': {
                title: 'Новелла 32',
                // type: SKIP,
            },
            '1:14:34': {
                title: 'Узнай что случилось с Варей через 10 лет',
                text: 'Узнай что случилось с Варей через 10 лет',
                type: COMBINED,
                image: 'p10',
                href: 'https://igra.film/',
                button: 'Смотреть',
            },
            '1:15:55': {
                title: 'Баба Маня сто лет назад',
                text: 'Баба Маня сто лет назад',
                type: COMBINED,
                image: 'p11',
                href: 'https://igra.film/',
                button: 'Смотреть',
            },
            '1:19:43': {
                title: 'Продолжение дружбы Бори и Жени',
                text: 'Продолжение дружбы Бори и Жени',
                type: COMBINED,
                image: 'p12',
                href: 'https://igra.film/',
                button: 'Смотреть',
            },
            '1:20:15': {
                title: 'Лыжник и сноубордист в армии',
                text: 'Лыжник и сноубордист в армии',
                type: COMBINED,
                image: 'p13',
                href: 'https://igra.film/',
                button: 'Смотреть',
            },
        },
    },
    {
        episode: '02',
        src: 'https://ongocinemamedia-euno.streaming.media.azure.net//15744a37-e2e2-4ece-b297-25595da93b03/YOLKI-2-25fps-ORIGINAL-1920x800-.ism/manifest',
        title: 'Елки 2',
        activeElements: {
            // '0:00:00': { // example fields
            //     button: '',
            //     title: '',
            //     text: '',
            //     type: '',
            //     duration: 3.00 || DEFAULT_DURATION,
            //     image: '',
            //     href: '',
            //     targetTime: '',
            //     width: 10.0000,
            //     left: 10.0000,
            //     top: 65.0000,
            // },
        },
    },
]

export default sequence;
